﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickHalfBlock : MonoBehaviour {

    public GameObject publicHalf1;
    public GameObject publicHalf2;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            //Debug.Log("VERTICAL ");
            Collide(collision);
            //collision.gameObject.GetComponent<BulletScript>().Kill();
        }
    }

    public void Collide(Collision2D collision)
    {
        GameObject temp;
        Vector2 OffsetPosition = GetComponent<Collider2D>().offset;
        Vector3 pos1;
        Vector3 pos2;
       
        if (OffsetPosition.x==0f)
        {
            pos1 = new Vector3(transform.position.x - 0.25f, transform.position.y + OffsetPosition.y, transform.position.z);
            pos2 = new Vector3(transform.position.x + 0.25f, transform.position.y + OffsetPosition.y, transform.position.z);
        }
        else
        {
            pos1 = new Vector3(transform.position.x + OffsetPosition.x, transform.position.y - 0.25f, transform.position.z);
            pos2 = new Vector3(transform.position.x + OffsetPosition.x, transform.position.y + 0.25f, transform.position.z);
        }
        
        temp = Instantiate(publicHalf1, pos1, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;

        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawn1" + Time.time);
            temp.GetComponent<BrickBlockSimple>().Collide(collision);
        }
        temp = Instantiate(publicHalf2, pos2, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;

        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawn2" + Time.time);
            temp.GetComponent<BrickBlockSimple>().Collide(collision);
        }
        Destroy(gameObject);
    }
}
