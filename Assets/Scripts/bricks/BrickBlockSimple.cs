﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickBlockSimple : MonoBehaviour {

    public GameObject publicHalfBrickTop;
    public GameObject publicHalfBrickBottom;
    public GameObject publicHalfBrickLeft;
    public GameObject publicHalfBrickRight;

    private Vector2 _privateMaxBound;

    private void Awake()
    {
        _privateMaxBound=new Vector2(GetComponent<BoxCollider2D>().size.x * 0.6f, GetComponent<BoxCollider2D>().size.y * 0.6f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Enter");
        Collide(collision);
    }

    public void Collide(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            //Vector3 hit = transform.position - collision.transform.position;
            Vector2 point = Vector2.zero;
            //Debug.Log("collision " + collision.contacts.Length);
            int ile = 0;
            for (int i = 0; i < collision.contacts.Length; ++i)
            {
                Vector2 a = new Vector2(transform.position.x - collision.contacts[i].point.x, transform.position.y - collision.contacts[i].point.y);
               // Debug.Log(a + " " + _privateMaxBound);
                if (Mathf.Abs(a.x) < _privateMaxBound.x && Mathf.Abs(a.y) <= _privateMaxBound.y)
                {
                    ile++;
                    point += a;//collision.contacts[i].point;
                }
            }
            //Debug.Log("contact: " + point);
            point = point / ile;
            //Debug.Log("contact rel: " + point);
            if (Mathf.Abs(point.x) >= Mathf.Abs(point.y))
            {
                SpawnH(collision);
            }
            else //if (Mathf.Abs(point.x) > Mathf.Abs(point.y))
            {
                SpawnV(collision);
            }
            /*
            else
            {
                //Debug.Log("tutaj");
                Vector3 pos = collision.gameObject.transform.position - transform.position;
                if (Mathf.Abs(pos.x)>Mathf.Abs(pos.y))
                {
                    SpawnH(collision);
                }
                else
                {
                    SpawnV(collision);
                }
            }*/
            Destroy(gameObject);
        }
    }

    public void SpawnV(Collision2D collision)
    {
        //Debug.Log("spawnVerticalSimple");
        GameObject temp;
        temp = Instantiate(publicHalfBrickTop, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;

        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawnVerticalSimpleTop " + Mathf.Abs(temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance));
            temp.GetComponent<Brick>().Collide(collision);
        }
        
        temp = Instantiate(publicHalfBrickBottom, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;

        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
           // Debug.Log("spawnVerticalSimpleBottom " + Mathf.Abs(temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance));
            temp.GetComponent<Brick>().Collide(collision);
        }
        
    }
    public void SpawnH(Collision2D collision)
    {
        //Debug.Log("spawnHorizontalSimple");
        GameObject temp;
        temp = Instantiate(publicHalfBrickLeft, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;

        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawnHorizontalSimpleLeft " + Mathf.Abs(temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance));
            temp.GetComponent<Brick>().Collide(collision);
        }
        
        temp = Instantiate(publicHalfBrickRight, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;
        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
           // Debug.Log("spawnHorizontalSimplRight " + Mathf.Abs(temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance));
            temp.GetComponent<Brick>().Collide(collision);
        }
        
    }
}
