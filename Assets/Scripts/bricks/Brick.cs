﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            Collide(collision);
        }
    }
    public void Collide(Collision2D collision)
    {
        //Debug.Log("Brick Kill " + Time.time);
        collision.gameObject.GetComponent<BulletScript>().setKilled();
        Destroy(gameObject);
    }

}
