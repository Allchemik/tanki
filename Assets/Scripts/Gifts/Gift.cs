﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gift : MonoBehaviour {

    public float publicTimeToFlash;
    public float publicTimeToEnd;

    private float _privateEndTime;
    private float _privateFlashingTime;

    private bool _privateFlashing;

    public void init()
    {
        GetComponent<Animator>().speed = 0;
        _privateEndTime = Time.time + publicTimeToEnd;
        _privateFlashingTime = _privateEndTime - publicTimeToFlash;
        _privateFlashing = false;
    }

    public void timeout()
    {
        if(Time.time>_privateFlashingTime&&_privateFlashing ==false)
        {
            _privateFlashing = true;
            GetComponent<Animator>().speed = 1;
            return;
        }
        if(Time.time>_privateEndTime)
        {
            Destroy(gameObject);
        }
    }
}
