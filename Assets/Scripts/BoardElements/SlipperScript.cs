﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlipperScript : MonoBehaviour {

    public float publicInteractionTime;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Enemy")|| collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Tank>().publicSlipping = true;
            collision.gameObject.GetComponent<Tank>().publicEndSlipTime = Time.time + publicInteractionTime;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Player"))
        {
            if( collision.gameObject.GetComponent <Rigidbody2D>().velocity!=Vector2.zero)
            {
                collision.gameObject.GetComponent<Tank>().publicSlipping = true;
                //collision.gameObject.GetComponent<Tank>().publicEndSlipTime = Time.time + publicInteractionTime;
            }
        }
    }

}
