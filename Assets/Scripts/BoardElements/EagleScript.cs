﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleScript : MonoBehaviour {

    public void Kill()
    {
        GetComponent<Animator>().SetTrigger("explode");
        GameControllerScript.Instance.KillEagle();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            Kill();
        }
    }
}
