﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour {


    public float publicTime=1;    
        
	// Update is called once per frame
	void Update ()
    {
        if(publicTime<=0)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            publicTime -= Time.deltaTime;
        }
	}



}
