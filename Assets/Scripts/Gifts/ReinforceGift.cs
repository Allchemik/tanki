﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReinforceGift : Gift {

    // Use this for initialization
    void Awake()
    {
        init();
    }
    // Update is called once per frame
    void Update()
    {
        timeout();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject Nest = GameObject.FindGameObjectWithTag("Nest");
            Debug.Log(Nest);
            Nest.GetComponent<NestScript>().reinforce();
            Destroy(gameObject);
        }
    }
}
