﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickBlock : MonoBehaviour {

    public GameObject publicHalfBrickTop;
    public GameObject publicHalfBrickBottom;
    public GameObject publicHalfBrickLeft;
    public GameObject publicHalfBrickRight;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Enter");
        Collide(collision);
    }

    public void Collide(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("EnemyBullet"))
        {
            //Vector3 hit = transform.position - collision.transform.position;
            Vector2 point= Vector2.zero;
            for (int i = 0; i < collision.contacts.Length; ++i)
            {
                Vector2 a = new Vector2(transform.position.x - collision.contacts[i].point.x, transform.position.y - collision.contacts[i].point.y);
                
                point += a;
            }
            point = point / collision.contacts.Length;

            if (Mathf.Abs(point.x) >= Mathf.Abs(point.y))
            {
                //Debug.Log("spawnHorizontal"+Time.time);
                SpawnH(collision);
            }
            else
            {
                //Debug.Log("spawnVertical" + Time.time);
                SpawnV(collision);
            }
            Destroy(gameObject);
        }
    }

    public void SpawnV(Collision2D collision)
    {
        GameObject temp;
        temp = Instantiate(publicHalfBrickTop, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;
        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance<=0f)
        {
            //Debug.Log("spawnVerticalTop" + Time.time);
            temp.GetComponent<BrickHalfBlock>().Collide(collision);
        }
        temp = Instantiate(publicHalfBrickBottom, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;
        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawnVerticalBottom" + Time.time);
            temp.GetComponent<BrickHalfBlock>().Collide(collision);
        }
    }
    public void SpawnH(Collision2D collision)
    {
        GameObject temp;
        temp = Instantiate(publicHalfBrickLeft, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;
        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawnHorizontalLeft" + Time.time);
            temp.GetComponent<BrickHalfBlock>().Collide(collision);
        }
        temp = Instantiate(publicHalfBrickRight, transform.position, transform.rotation) as GameObject;
        temp.transform.parent = transform.parent;
        if (temp.GetComponent<Collider2D>().Distance(collision.gameObject.GetComponent<Collider2D>()).distance <= 0f)
        {
            //Debug.Log("spawnHorizontalRight" + Time.time);
            temp.GetComponent<BrickHalfBlock>().Collide(collision);
        }
    }
}
