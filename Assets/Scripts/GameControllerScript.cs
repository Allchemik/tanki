﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControllerScript : MonoBehaviour {

    public static GameControllerScript Instance = null;
    public Transform[] publicEnemySpawnPoints;
    public Transform publicPlayerSpawnPoint;

    public GameObject[] publicTanki;
    public GameObject[] publicGifts;

    public int publicNumberGifts;
    private int _privateSpawnedGifs;

    public GameObject publicSpawnAnim;
    public GameObject PublicPlayerModel;
    public GameObject publicPlayer;

    public GameObject[] LvlLetters;
    public GameObject[] Player1_Lifes;
    public GameObject[] Player2_Lifes;

    public Sprite[] Letters;

    public int publicMaxTanks;
    public int publicMaxTanksOnBoard;
    
    private int _privateTanksCount;
    private int _privateTanksOnBoard;

    private bool EndGame;

    private void Awake()
    {
       // Physics2D.IgnoreLayerCollision(8, 9);
        //Physics2D.IgnoreLayerCollision(10, 11);
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        //DontDestroyOnLoad(gameObject);

        InitGame();
    }

    // Use this for initialization
    public void InitGame()
    {
        _privateTanksCount = 0;
        _privateTanksOnBoard = 0;
        ShuffleTanks(publicTanki);
        EndGame = false;
        KillPlayer();
        
        StartCoroutine(spawnControl());
    }

    void ShuffleTanks(GameObject[] arr)
    {
        for (int i = arr.Length - 1; i > 0; i--)
        {
            int r = Random.Range(0, i);
            GameObject tmp = arr[i];
            arr[i] = arr[r];
            arr[r] = tmp;
        }
    }


    public IEnumerator spawnControl()
    {
        float time;
        while (!EndGame)
        {
            time = Random.Range(1f, 3f);
            yield return new WaitForSeconds(time);
            SpawnTank();
        }
    }


    private void checkEndGame()
    {
        if(EndGame)
        {
            //Application.LoadLevel(0);
            GameController.Instance.EndGame();
        }
    }
    
    public void SpawnTank()
    {
        if (_privateTanksCount < publicMaxTanks)
        {
            if (_privateTanksOnBoard < publicMaxTanksOnBoard)
            {
                int spawnPoint = Mathf.RoundToInt(Random.Range(0, publicEnemySpawnPoints.Length-1));
                //int TankType = Mathf.RoundToInt(Random.Range(0, publicTanki.Length-1));
                int GiftType = Mathf.RoundToInt(Random.Range(0, publicGifts.Length - 1));
                float roll = Random.Range(0f, 1f);
                
                _privateTanksCount++;
                _privateTanksOnBoard++;

                GameObject temp =  Instantiate(publicSpawnAnim, publicEnemySpawnPoints[spawnPoint]) as GameObject;
                GameObject TankToSpawn = publicTanki[_privateTanksCount-1];
                if (_privateSpawnedGifs < publicNumberGifts && roll >= 0.5f)
                {
                    Debug.Log(" prezent");
                    //TankToSpawn.GetComponent<EnemyScript>().publicGift = publicGifts[GiftType];
                    _privateSpawnedGifs++;
                    temp.GetComponent<SpawnScript>().SetElementToSpawn(TankToSpawn, publicGifts[GiftType]);
                }
                else
                {
                    temp.GetComponent<SpawnScript>().SetElementToSpawn(TankToSpawn);
                }

                UpdateTankGui();
            }
        }
    }
    public void killTank()
    {
        _privateTanksOnBoard--;
        if (_privateTanksOnBoard <= 0 && _privateTanksCount >= publicMaxTanks)
        {
            EndGame = true;
            checkEndGame();
        }
    }
    public void KillPlayer()
    {
        GameController.Instance.publicPlayerLives--;
        if(GameController.Instance.publicPlayerLives<= 0)
        {
            EndGame = true;
            checkEndGame();
        }
        else
        {
           GameObject temp =  Instantiate(PublicPlayerModel, publicPlayerSpawnPoint) as GameObject;
        }
        UpdatePlayerLive(GameController.Instance.publicPlayerLives);
    }

    public void KillEagle()
    {
        GameController.Instance.publicEagle = false;
        EndGame = true;
        checkEndGame();
    }

    public void UpdatePlayerLive(int Lives)
    {
        int dec = Lives / 10;
        int rest = Lives % 10;
        if (dec > 0)
        {
            Player1_Lifes[0].GetComponent<SpriteRenderer>().sprite = Letters[dec];
            Player1_Lifes[1].GetComponent<SpriteRenderer>().sprite = Letters[rest];
        }
        else
        {
            Player1_Lifes[0].GetComponent<SpriteRenderer>().sprite = Letters[rest];
        }
    }

    public void UpdateLvl(int Lvl)
    {
        int dec = Lvl / 10;
        int rest = Lvl % 10;
        if (dec > 0)
        {
            LvlLetters[0].GetComponent<SpriteRenderer>().sprite = Letters[dec];
            LvlLetters[1].GetComponent<SpriteRenderer>().sprite = Letters[rest];
        }
        else
        {
            LvlLetters[0].GetComponent<SpriteRenderer>().sprite = Letters[rest];
        }
    }
    public void UpdateTankGui()
    {
        GameObject[] tanki = GameObject.FindGameObjectsWithTag("TankSymbol");
        for(int i=0;i<_privateTanksCount;++i)
        {
            tanki[i].GetComponent<SpriteRenderer>().enabled = false;
            tanki[i].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
