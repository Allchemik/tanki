﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : Tank {

    public GameObject publicGift;

    public bool publicFreeze;


    // Use this for initialization
    void Awake () {
        _PrivateRigidbody = GetComponent<Rigidbody2D>();

        transform.rotation = Quaternion.Euler(0f, 0f, 180f);

        _privateState = Tank.State.bottom;
        _privateDirection = new Vector2(0f, -1f);
        transform.rotation = Quaternion.Euler(0f, 0f, 180f);
        _privateManeuver = Maneuver.bottom;
        _privateUpdateManeuver = true;
        _privateAnimator = GetComponent<Animator>();
        publicFreeze = false;
        // Physics2D.IgnoreLayerCollision(8, 10);
        ApplyManeuver();
        StartCoroutine(newManeuver());
	}

	// Update is called once per frame
	void Update ()
    {
        UpdateManeuver();
        ApplyManeuver();
        ApplyFire();
        Move();
    }

protected void UpdateManeuver()
    {
        switch (_privateManeuver)
        {
            case Maneuver.top:
                if (_privateState != State.top)
                {
                    _privateUpdateManeuver = true;
                }
                else
                {
                    _privateFire = true;
                }
                break;
            case Maneuver.bottom:
                if (_privateState != State.bottom)
                {
                    _privateUpdateManeuver = true;
                }
                else
                {
                    _privateFire = true;
                }
                break;
            case Maneuver.right:
                if (_privateState != State.right)
                {
                    _privateUpdateManeuver = true;
                }
                else
                {
                    _privateFire = true;
                }
                break;
            case Maneuver.left:
                if (_privateState != State.right)
                {
                    _privateUpdateManeuver = true;
                }
                else
                {
                    _privateFire = true;
                }
                break;
        }
    }



    IEnumerator newManeuver()
    {
        float time;
        int maneuver;
        while(!publicFreeze)
        {
            time = Random.Range(1f, 3f);
            maneuver = Mathf.RoundToInt(Random.Range(-0.5f, 4.5f));
            _privateUpdateManeuver = true;
            _privateDrive = true;
            switch(maneuver)
            {
                case 0:
                    _privateManeuver = Maneuver.top;
                break;
                case 1:
                    _privateManeuver = Maneuver.left;
                break;
                case 2:
                    _privateManeuver = Maneuver.bottom;
                break;
                case 3:
                    _privateManeuver = Maneuver.right;
                break;
                case 4:
                    _privateManeuver = Maneuver.fire;
                break;
            }
            yield return new WaitForSeconds(time);
        }
    }

    override protected void Kill()
    {
        GameObject temp = Instantiate(publicExplosion, transform.position, Quaternion.identity) as GameObject;
        temp.transform.parent = null;
        GameControllerScript.Instance.killTank();
        if (publicGift != null)
        {
            temp = Instantiate(publicGift, transform.position, Quaternion.identity) as GameObject;
            temp.transform.parent = null;
        }
        Destroy(gameObject);
    }

    override public bool TakeDamage(int damage)
    {
        publicHP -= damage;
        if(publicHP<=0)
        {
            Kill();
            return true;
        }
        return false;
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Bullet"))
        {
            collision.gameObject.GetComponent<BulletScript>().Kill();
            Kill();
            Destroy(collision.gameObject);
        }
        /*else if(collision.gameObject.CompareTag("EnemyBullet"))
        {
            collision.gameObject.GetComponent<BulletScript>().Kill();
        }
    }*/
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Enemy")||collision.CompareTag("Player"))
        {
            _privateDrive = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("Player"))
        {
            _privateDrive = true;
        }
    }
    public void Freeze()
    {
        publicFreeze = true;
        StopCoroutine(newManeuver());
    }
    public void unFreeze()
    {
        publicFreeze = false;
        StartCoroutine(newManeuver());
    }

    public void flash()
    {
        if (publicGift != null)
        {
            _privateAnimator.SetBool("Flash", true);
        }
    }
}
