﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NestScript : MonoBehaviour {

    Vector3[] spawnPoints = { new Vector3(1.5f, 1f, 0f), new Vector3(1f, 2f, 0f), new Vector3(0f, 2f, 0f), new Vector3(-0.5f, 1f, 0f) };
    public GameObject Brick_Left;
    public GameObject Brick_Right;
    public GameObject Brick_Bottom;

    public GameObject Concrete_Left;
    public GameObject Concrete_Right;
    public GameObject Concrete_Bottom;

    public float publicTimeToFlash;
    public float publicTimeToEnd;

    private float _privateEndTime;
    private float _privateFlashingTime;

    // Use this for initialization
    void Awake() {
        //Brick_Left = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Brick/Brick_Block_Left.prefab");
        //Brick_Right = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Brick/Brick_Block_Right.prefab");
        //Brick_Bottom = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Brick/Brick_Block_Bottom.prefab");

        //Concrete_Left = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Concrete/Concrete_Block_Left.prefab");
        //Concrete_Right = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Concrete/Concrete_Block_Right.prefab");
        //Concrete_Bottom = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Obstacles/Concrete/Concrete_Block_Bottom.prefab");
    }

    // Update is called once per frame
    void Update() {

    }
    public void reinforce()
    {
        _privateEndTime = Time.time + publicTimeToEnd;
        _privateFlashingTime = _privateEndTime - publicTimeToFlash;
        ClearNest();
        SpawnConcrete();
        StartCoroutine(CountDown());
    }
    void ClearNest()
    {
        for (int i = 0; i < transform.childCount; ++i)
        {
            if (transform.GetChild(i).CompareTag("Brick") || transform.GetChild(i).CompareTag("Obstacles"))
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
    void SpawnBricks()
    {
        GameObject Left = Instantiate(Brick_Left, spawnPoints[0], Quaternion.identity) as GameObject;
        Left.transform.parent = transform;
        GameObject Bottom1 = Instantiate(Brick_Bottom, spawnPoints[1], Quaternion.identity) as GameObject;
        Bottom1.transform.parent = transform;
        GameObject Bottom2 = Instantiate(Brick_Bottom, spawnPoints[2], Quaternion.identity) as GameObject;
        Bottom2.transform.parent = transform;
        GameObject Right = Instantiate(Brick_Right, spawnPoints[3], Quaternion.identity) as GameObject;
        Right.transform.parent = transform;
    }
    void SpawnConcrete()
    {
        GameObject Left = Instantiate(Concrete_Left, spawnPoints[0], Quaternion.identity) as GameObject;
        Left.transform.parent = transform;
        GameObject Bottom1 = Instantiate(Concrete_Bottom, spawnPoints[1], Quaternion.identity) as GameObject;
        Bottom1.transform.parent = transform;
        GameObject Bottom2 = Instantiate(Concrete_Bottom, spawnPoints[2], Quaternion.identity) as GameObject;
        Bottom2.transform.parent = transform;
        GameObject Right = Instantiate(Concrete_Right, spawnPoints[3], Quaternion.identity) as GameObject;
        Right.transform.parent = transform;
    }
    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(_privateFlashingTime);
        while(Time.time<_privateEndTime)
        {
            ClearNest();
            SpawnBricks();
            yield return new WaitForSeconds(0.25f);
            ClearNest();
            SpawnConcrete();
            yield return new WaitForSeconds(0.25f);
        }
        ClearNest();
        SpawnBricks();
    }
}
