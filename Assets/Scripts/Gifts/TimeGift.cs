﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGift : Gift
{
    public float publicFreezeTime;
    // Use this for initialization
    void Awake()
    {
        init();
    }
    // Update is called once per frame
    void Update()
    {
        timeout();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject[] Tanks = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject tank in Tanks)
            {
                tank.GetComponent<EnemyScript>().Freeze();
            }
            Destroy(gameObject);
        }
    }
    IEnumerator Froze()
    {
        yield return new WaitForSeconds(publicFreezeTime);
        GameObject[] Tanks = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject tank in Tanks)
        {
            tank.GetComponent<EnemyScript>().unFreeze();
        }
    }
}
