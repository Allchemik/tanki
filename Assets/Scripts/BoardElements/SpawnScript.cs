﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

    private GameObject _privateTank;

	public void SetElementToSpawn(GameObject tank)
    {
        _privateTank = tank;
        StartCoroutine(Spawn());
    }
    public void SetElementToSpawn(GameObject tank,GameObject prezent)
    {
        _privateTank = tank;
        StartCoroutine(Spawn(prezent));
    }
    public IEnumerator Spawn()
    {
        yield return new WaitForSeconds(0.6f);
        GameObject temp = Instantiate(_privateTank, transform.position,transform.rotation) as GameObject;
        //temp.transform.parent = null;
        DestroyImmediate(gameObject);
    }

    public IEnumerator Spawn(GameObject prezent)
    {
        yield return new WaitForSeconds(0.6f);
        GameObject temp = Instantiate(_privateTank, transform.position, transform.rotation) as GameObject;
        temp.GetComponent<EnemyScript>().publicGift = prezent;
        temp.GetComponent<EnemyScript>().flash();
        //temp.transform.parent = null;
        DestroyImmediate(gameObject);
    }
}

