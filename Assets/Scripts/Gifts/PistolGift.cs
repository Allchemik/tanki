﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolGift : Gift {

    // Use this for initialization
    void Awake()
    {
        init();
    }
    // Update is called once per frame
    void Update()
    {
        timeout();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerScript>().foundStar();
            collision.gameObject.GetComponent<PlayerScript>().foundStar();
            collision.gameObject.GetComponent<PlayerScript>().foundStar();
            Destroy(gameObject);
        }
    }
}
