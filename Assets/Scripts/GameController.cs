﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public static GameController Instance = null;
    public int publicPlayerLives;
    public int publicCurrentLvl;
    public bool publicEagle;

    // Use this for initialization
    void Awake () {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void NewGame()
    {
        publicPlayerLives = 3;
        publicCurrentLvl =1;
        publicEagle = true;
        //SceneManager.UnloadScene(0);
        SceneManager.LoadScene(publicCurrentLvl);
        //load lvl 1;
    }
    public void EndGame()
    {
        if (publicEagle)
        {
            if (publicPlayerLives > 0)
            {
                publicPlayerLives++;
                publicCurrentLvl++;
                SceneManager.LoadScene(publicCurrentLvl);
                //load next lvl;
            }
            else
            {
                publicCurrentLvl = 0;
                SceneManager.LoadScene(publicCurrentLvl);
                //back to menu;
            }
        }
        else
        {
            publicCurrentLvl = 0;
            SceneManager.LoadScene(publicCurrentLvl);
            //back to menu;
        }
    }
}
