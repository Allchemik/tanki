﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public float publicSpeed;
    public int publicDamage;
    public GameObject publicExplosion;
    private Rigidbody2D _privateRigidbody;
    private bool _privateIsKilled=false;

    private void Awake()
    {
        _privateRigidbody = GetComponent<Rigidbody2D>();
//        Physics2D.IgnoreLayerCollision(8, 10);
    }

    private void Update()
    {
        if (_privateIsKilled)
            Kill();
    }
    public void setKilled()
    {
        _privateIsKilled = true;
    }
    public void setSpeed(Vector2 direction)
    {
         _privateRigidbody.velocity = direction * publicSpeed;
    }
    public void Kill()
    {
        GameObject temp = Instantiate(publicExplosion, transform)as GameObject;
        temp.transform.parent = null;
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bound"))
        {
            Kill();
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Tank>().TakeDamage(publicDamage);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<Tank>().TakeDamage(publicDamage);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Obstacles"))
        {
            Kill();
        }
        if(collision.gameObject.CompareTag("Brick"))
        {
            //Kill();
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
