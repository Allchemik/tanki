﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerScript : Tank {

    public GameObject publicShield;
    private GameObject _privateShield;

    public float publicShieldTime;
    private int _privateLvl;

    void Awake() {
        _privateAnimator = GetComponent<Animator>();
        _PrivateRigidbody = GetComponent<Rigidbody2D>();

        ActivateShield(publicShieldTime);

        _privateState = Tank.State.top;
        _privateLvl = 1;
    }

    // Update is called once per frame
    void Update()
    {
        GetControl();
        UpdateManeuver();
        ApplyManeuver();
        Move();
        ApplyFire();

        return;
    }

    public void ActivateShield(float time)
    {
        _privateShield = Instantiate(publicShield) as GameObject;
        _privateShield.transform.position = this.transform.position;
        _privateShield.transform.parent = this.transform;

        _privateShield.GetComponent<ShieldScript>().publicTime = time;
    }

    override public bool TakeDamage(int damage)
    {
        if (_privateShield == null)
        {
            publicHP -= damage;
            if (publicHP <= 0)
            {
                Kill();
                return true;
            }
        }
        return false;
    }

    override protected void Kill()
    {
        Instantiate(publicExplosion, transform);
        Destroy(gameObject);
        GameControllerScript.Instance.KillPlayer();
    }

    private void GetControl()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        if (Input.GetButton("Fire1"))
        {
            _privateFire = true;
        }
        if (v > 0.1f)
        {
            _privateManeuver = Maneuver.top;
            _privateDrive = true;
            return;
        }
        else if (v < -0.1f)
        {
            _privateManeuver = Maneuver.bottom;
            _privateDrive = true;
            return;
        }
        if (h > 0.1f)
        {
            _privateManeuver = Maneuver.right;
            _privateDrive = true;
            return;
        }
        else if (h < -0.1f)
        {
            _privateManeuver = Maneuver.left;
            _privateDrive = true;
            return;
        }
        _privateDrive = false;

    }

    public void foundStar()
    {
        _privateLvl++;
        _privateLvl %= 5;
        switch(_privateLvl)
        {
            case 2:
                GetComponent<Animator>().SetTrigger("Small");
                publicReloadTime = publicReloadTime / 2;
                break;
            case 3:
                GetComponent<Animator>().SetTrigger("Med");
                publicReloadTime = publicReloadTime / 2;
                publicDamage = 2;
                break;
            case 4:
                GetComponent<Animator>().SetTrigger("Big");
                publicHP = 4;
                publicDamage = 4;
                break;
        }
    }
}
