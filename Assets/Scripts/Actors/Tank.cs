﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Maneuver
{
    top,
    bottom,
    left,
    right,
    fire
};

public abstract class Tank : MonoBehaviour {

    public enum State
    {
        top,
        left,
        bottom,
        right
    };

    public int publicHP;
    public int publicDamage;
    public float publicSpeed;
    public float publicReloadTime;

    [HideInInspector]
    public float publicEndSlipTime;
    [HideInInspector]
    public bool publicSlipping=false;

    public GameObject publicExplosion;
    public GameObject publicBullet;

    protected State _privateState;
    protected Maneuver _privateManeuver;
    protected bool _privateFire;
    protected bool _privateUpdateManeuver;
    protected bool _privateDrive;
    protected float _privateReloadTimer;

    protected Animator _privateAnimator;
    protected Rigidbody2D _PrivateRigidbody;

    protected Vector2 _privateDirection;

    protected void ApplyFire()
    {
        if (_privateFire)
        {
            _privateFire = false;
            if (Time.time > _privateReloadTimer)
            {
                _privateReloadTimer = Time.time + publicReloadTime;
                GameObject temp = Instantiate(publicBullet, transform.position, transform.rotation) as GameObject;
                temp.GetComponent<BulletScript>().publicDamage = publicDamage;
                Physics2D.IgnoreCollision(GetComponent<Collider2D>(), temp.GetComponent<Collider2D>());
                switch (_privateState)
                {
                    case State.top:
                        temp.GetComponent<BulletScript>().setSpeed(new Vector2(0f, 1f));
                        break;
                    case State.bottom:
                        temp.GetComponent<BulletScript>().setSpeed(new Vector2(0f, -1f));
                        break;
                    case State.left:
                        temp.GetComponent<BulletScript>().setSpeed(new Vector2(-1f, 0f));
                        break;
                    case State.right:
                        temp.GetComponent<BulletScript>().setSpeed(new Vector2(1f, 0f));
                        break;
                }
            }
        }
    }

    protected void ApplyManeuver()
    {
        if (_privateUpdateManeuver)
        {
            _privateAnimator.SetTrigger("Drive");
            switch (_privateManeuver)
            {
                case Maneuver.top:
                    _PrivateRigidbody.constraints = RigidbodyConstraints2D.FreezePositionX;
                    _privateState = Tank.State.top;
                    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                    _privateDirection = new Vector2(0f, publicSpeed);
                    break;
                case Maneuver.bottom:
                    _privateState = Tank.State.bottom;
                    _PrivateRigidbody.constraints = RigidbodyConstraints2D.FreezePositionX;
                    transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                    _privateDirection = new Vector2(0f, -publicSpeed);

                    break;
                case Maneuver.right:
                    _PrivateRigidbody.constraints = RigidbodyConstraints2D.FreezePositionY;
                    _privateState = Tank.State.right;
                    transform.rotation = Quaternion.Euler(0f, 0f, 270f);
                    _privateDirection = new Vector2(publicSpeed, 0f);
                    break;
                case Maneuver.left:
                    _PrivateRigidbody.constraints = RigidbodyConstraints2D.FreezePositionY;
                    _privateState = Tank.State.left;
                    transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                    _privateDirection = new Vector2(-publicSpeed, 0f);
                    break;
            }
        }
    }

    protected void UpdateManeuver()
    {
        switch (_privateManeuver)
        {
            case Maneuver.top:
                if (_privateState != State.top)
                {
                    _privateUpdateManeuver = true;
                }
                break;
            case Maneuver.bottom:
                if (_privateState != State.bottom)
                {
                    _privateUpdateManeuver = true;
                }
                break;
            case Maneuver.right:
                if (_privateState != State.right)
                {
                    _privateUpdateManeuver = true;
                }
                break;
            case Maneuver.left:
                if (_privateState != State.right)
                {
                    _privateUpdateManeuver = true;
                }
                break;
        }
    }

    protected void Move()
    {
        isSlipping();
        if (_privateDrive)
        {
            _privateAnimator.speed = 1;
            //_PrivateRigidbody.MovePosition(_PrivateRigidbody.position + (_privateDirection * Time.deltaTime));
            _PrivateRigidbody.velocity = _privateDirection * publicSpeed;
        }
        else
        {
            _privateAnimator.speed = 0;
            if (!publicSlipping)
            {
                _PrivateRigidbody.velocity = Vector2.zero;
            }
        }
    }

    protected void isSlipping()
    {
        bool temp = publicSlipping;
        publicSlipping = Time.time < publicEndSlipTime;
        if(publicSlipping!= temp)
        {
            Debug.Log("slip");
        }
    }

    abstract protected void Kill();

    abstract public bool TakeDamage(int damage);

}
