﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TilesAlignerWindow : EditorWindow
{
    private const int size = 75;
    /*
    private const string TreeImagePath = "Assets/Sprites/elementy w grze/drzewo.png";
    private const string TreeAssetPath = "Assets/Resources/MapElements/Tree.prefab";
    static Texture treeTex;
    
    private const string FenceImagePath = "Assets/Sprites/elementy w grze/płotek.png";
    private const string FenceAssetPath = "Assets/Resources/MapElements/Fence.prefab";
    static Texture fenceTex;

    private const string HoleImagePath = "Assets/Sprites/elementy w grze/dziura.png";
    private const string HoleAssetPath = "Assets/Resources/MapElements/Hole.prefab";
    static Texture holeTex;

    private const string GroundImagePath = "Assets/Sprites/Old/GrassTile.png";
    private const string GroundAssetPath = "Assets/Resources/MapElements/Ground.prefab";
    static Texture groundTex;

    private const string ChickenImagePath = "Assets/Sprites/elementy w grze/pisklak spritesheet.png";
    private const string ChickenAssetPath = "Assets/Prefabs/chicken.prefab";
    static Texture chickenTex;
    */
    [MenuItem("Window/TileAligner")]
    public static void ShowWindow()
    {
        GetWindow<TilesAlignerWindow>();
    }

    void Awake()
    {
        //UpdateTextures();
    }

    void UpdateTextures()
    {
     /*   treeTex = AssetDatabase.LoadAssetAtPath<Texture>(TreeImagePath);
        fenceTex = AssetDatabase.LoadAssetAtPath<Texture>(FenceImagePath);
        holeTex = AssetDatabase.LoadAssetAtPath<Texture>(HoleImagePath);
        groundTex = AssetDatabase.LoadAssetAtPath<Texture>(GroundImagePath);
        */
        //  Object[] texs = AssetDatabase.LoadAllAssetsAtPath(ChickenImagePath);
        // chickenTex = texs[0] as Texture;
    }

    void OnInspectorUpdate()
    {
      //  UpdateTextures();
       // Repaint();
    }

    void OnGUI()
    {

     /*  if (GUILayout.Button(treeTex, GUILayout.Width(size), GUILayout.Height(size)))
        {
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(TreeAssetPath);
            Selection.activeObject = obj;
            //  GameObject clone = Instantiate(obj, Vector3.zero, Quaternion.identity) as GameObject;
        }


        if (GUILayout.Button(fenceTex, GUILayout.Width(size), GUILayout.Height(size)))
        {
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(FenceAssetPath);
            Selection.activeObject = obj;
            // GameObject clone = Instantiate(obj, Vector3.zero, Quaternion.identity) as GameObject;
            // Modify the clone to your heart's content
            // clone.transform.position = Vector3.one;
        }

        if (GUILayout.Button(holeTex, GUILayout.Width(size), GUILayout.Height(size)))
        {
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(HoleAssetPath);
            Selection.activeObject = obj;
        }

        if (GUILayout.Button(groundTex, GUILayout.Width(size), GUILayout.Height(size)))
        {
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(GroundAssetPath);
            Selection.activeObject = obj;
        }

        if (GUILayout.Button("reszta", GUILayout.Width(size), GUILayout.Height(size)))
        {
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(ChickenAssetPath);
            Selection.activeObject = obj;
        }
        */
        if (GUILayout.Button("Align tiles"))
        {
            GameObject[] fields = GameObject.FindGameObjectsWithTag("Obstacles");
            foreach (GameObject field in fields)
            {
                Vector3 ftp = field.transform.position;
                field.transform.position = new Vector3(Mathf.RoundToInt(ftp.x), Mathf.RoundToInt(ftp.y), Mathf.RoundToInt(ftp.z));
            }
            fields = GameObject.FindGameObjectsWithTag("Brick");
            foreach (GameObject field in fields)
            {
                Vector3 ftp = field.transform.position;
                field.transform.position = new Vector3(Mathf.RoundToInt(ftp.x), Mathf.RoundToInt(ftp.y), Mathf.RoundToInt(ftp.z));
            }
        }
    }
}